
export function Product(ID, NAME, PRICE, SCREEN, BACKCAMERA, FRONTCAMERA, IMG, DESC, TYPE) {
    this.id = ID;
    this.productName = NAME;
    this.price = PRICE;
    this.screen = SCREEN;
    this.backCamera = BACKCAMERA;
    this.frontCamera = FRONTCAMERA;
    this.img = IMG;
    this.desc = DESC;
    this.type = TYPE;
    this.TOTALPRICE = (quantity) => {
        return this.price * quantity;
    }
}