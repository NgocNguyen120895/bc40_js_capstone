import { Product } from "./model.js";

let PRODUCTS_ARR = [];
let CART = [];
fetchData();




function fetchData() {
    axios({
        url: `https://63b2c9ae5e490925c521a862.mockapi.io/food`,
        method: "GET",

    }).then(function (res) {
        PRODUCTS_ARR = res.data;
        renderProducts(PRODUCTS_ARR);

    }).catch(function (err) {
        console.log(err);
    })
}

function addItem(id) {
    let vitri = findindex(id, PRODUCTS_ARR);
    let qty = 1;
    let isExisting = false;
    PRODUCTS_ARR.forEach((i, index) => {
        if (vitri == index) {
            if (CART.length === 0) {
                const cartItem = {
                    id: i.id, productName: i.name, price: i.price, img: i.img, qty,
                    total: i.price * qty,
                };

                CART.push(cartItem);
            } else {
                CART.map(item => {
                    if (i.name === item.productName) {
                        item.qty += 1;
                        item.total = item.qty * i.price;
                        const cartItem = {
                            id: i.id, productName: i.name, price: i.price, img: i.img, qty: item.qty,
                            total: item.total,
                        };
                        isExisting = true;
                    }
                });
                if (isExisting == false) {
                    const cartItem = {
                        id: i.id, productName: i.name, price: i.price, img: i.img, qty,
                        total: i.price * qty,
                    };
                    CART.push(cartItem)
                }
            }
        }

    });
    renderCurrentCart(CART);
    saveLocal(CART);

    const sum = CART.reduce((acu, obj) => {
        return acu + obj.total;
    }, 0)
    document.getElementById('Total').innerHTML = sum;
};


function saveLocal(arr) {
    let JSON_CART = JSON.stringify(arr);
    localStorage.setItem("CART", JSON_CART);
    return JSON_CART
}



function renderCurrentCart(arr) {
    var cartHTML = "";
    arr.forEach((item, index) => {
        var cart = `
        <div>
        <div class="item">
            <div class="img">
            <img src="${item.img}">
        </div>
        <div class="content">
            <div class="tittle">Tên SP: ${item.productName}</div>
            <div class="price">Giá: ${item.total}</div>
            <span>Số lượng: 
            ${item.qty}
            </span>
            <div class="btncontroller">
                <button class="remove" id="remove" onclick='removeCartItem(${index})'><i class="fa fa-trash-alt"></i></button>
            </div>
        </div>
        <hr>
        </div>
        `
        cartHTML += cart;
    })
    document.getElementById('cartitem').innerHTML = cartHTML;
}



let changeQuantity = () => {
    document.getElementById('quantity').addEventListener('change', () => {
        let val = document.getElementById('quantity').value;
        console.log(val)
        return val
    });
}


let removeCartItem = (id) => {
    CART.splice(id, 1);
    renderCurrentCart(CART);
    saveLocal(CART)
}

function renderProducts(arr) {
    var contentHTML = "";
    arr.forEach(function (product) {
        var div = `
          <div class="item">
            <div class="img">
            <img src="${product.img}">
          </div>
          <div class="content">
            <div class="tittle">${product.name}</div>
            <div class="decs">Mô tả: ${product.desc}</div>
            <div class="price">Giá: ${product.price}</div>
            <div class="details">
            <span>${product.screen}</span> <br>
            <span>${product.backCamera}</span>  <br>
            <span>${product.frontCamera}</span> 
            </div>
            <div class="btncontroller">
              <button class="add" onclick="addItem(${product.id})">Add to cart</button>
            </div>
          </div>
         
        </div>`;
        return contentHTML += div;
    })

    document.getElementById('list').innerHTML = contentHTML;
}


function findindex(id, value) {
    var index = -1;
    for (var i = 0; i < value.length; i++) {
        var seekLocation = value[i];
        if (seekLocation.id == id) {
            index = i;
            break;
        }
    }
    return index;
}

let JSONCART = localStorage.getItem("CART");
if (JSONCART != null) {
    CART = JSON.parse(JSONCART).map(function (item) {
        return new Product(item.id, item.name, item.price, item.screen, item.backCamera, item.frontCamera, item.img, item.desc, item.type)
    })
    renderCurrentCart(CART)
}   



// let checkQtyCheck = () => {
//     console.log("test")
// }

window.removeCartItem = removeCartItem;
window.addItem = addItem;