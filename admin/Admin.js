import { Product } from "../js/model.js";

const URL = `https://63b2c9ae5e490925c521a862.mockapi.io/food`
let PRODUCTS_ARR = [];

let fetch = () => {
    axios({
        url: `${URL}`,
        method: "GET",

    }).then(function (res) {
        PRODUCTS_ARR = res.data;
        renderData(PRODUCTS_ARR);

    }).catch(function (err) {
        console.log(err);
    })
}
fetch()

let addNewProduct = () => {
    let data = getData();
    axios({
        url: `${URL}`,
        method: 'POST',
        data,
    }).then((res) => {
        alert("Them thanh cong!")
        fetch();
        clearValue();
    }).catch((err) => {
        console.log(err);
    });
}

let editData = (id) => {

    axios({
        url: `${URL}/${id}`,
        method: "GET",
    }).then((res) => {
        console.log(res);
        display(res.data);
    }).catch((err) => {
        console.log(err);
    });
}

let update = () => {
    let data = getData();
    axios({
        url: `${URL}/${data.id}`,
        method: "PUT",
        data,
    }).then((res) => {
        alert("Cap nhat thanh cong!")
        fetch();
        clearValue();
    })
        .catch((err) => {
            console.log(err);
        });
}
let removeData = (id) => {
    axios({
        url: `${URL}/${id}`,
        method: 'DELETE',
    }).then((res) => {
        alert("Xoa thanh cong!");
        fetch();
    }).catch((err) => {
        console.log(err);
    });
}

let getData = () => {
    let id = document.getElementById('product-id').value;
    let name = document.getElementById('product-name').value;
    let price = document.getElementById('product-price').value;
    let screen = document.getElementById('product-screen').value;
    let bCam = document.getElementById('product-backCam').value;
    let fCam = document.getElementById('product-frontCam').value;
    let img = document.getElementById('product-img').value;
    let desc = document.getElementById('product-desc').value;
    let type = document.getElementById('product-type').value;

    return new Product(id, name, price, screen, bCam, fCam, img, desc, type)
}

let renderData = (data) => {
    let contentHTML = "";
    data.forEach(function (item) {
        var content = /*html*/ `
        <tr>
        <td>${item.id}</td>
        <td>${item.name}</td>
        <td>${item.price}</td>
        <td>${item.screen}</td>
        <td>${item.backCamera}</td>
        <td>${item.frontCamera}</td>
        <td>
        <img class="w-75 h-100 " src="${item.img}">
        </td>
        <td>${item.desc}</td>
        <td>${item.type}</td>
            <td>
                <button id="btn-del" class="btn btn-danger" onclick="removeData('${item.id}')">Xoa</button>
                <button class="btn btn-secondary" onclick="editData('${item.id}')">Sua</button>
            </td>
        </tr>`
        contentHTML += content;
    });
    return document.getElementById('tbody').innerHTML = contentHTML;
}

let display = (data) => {
    document.getElementById('product-id').value = data.id;
    document.getElementById('product-name').value = data.name;
    document.getElementById('product-price').value = data.price;
    document.getElementById('product-screen').value = data.screen;
    document.getElementById('product-backCam').value = data.backCamera;
    document.getElementById('product-frontCam').value = data.frontCamera;
    document.getElementById('product-img').value = data.img;
    document.getElementById('product-desc').value = data.desc;
    document.getElementById('product-type').value = data.type;
}

let clearValue = (data) => {
    document.getElementById('product-id').value = "";
    document.getElementById('product-name').value = "";
    document.getElementById('product-price').value = "";
    document.getElementById('product-screen').value = "";
    document.getElementById('product-backCam').value = "";
    document.getElementById('product-frontCam').value = "";
    document.getElementById('product-img').value = "";
    document.getElementById('product-desc').value = "";
    document.getElementById('product-type').value = "";
}


document.getElementById('btnAdd').addEventListener('click', addNewProduct);
document.getElementById('btnCapNhat').addEventListener('click', update);

window.removeData = removeData;
window.editData = editData;